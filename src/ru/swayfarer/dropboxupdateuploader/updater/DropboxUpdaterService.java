package ru.swayfarer.dropboxupdateuploader.updater;

import com.dropbox.core.v2.DbxClientV2;

import lombok.NonNull;
import ru.swayfarer.dropboxupdateuploader.gui.GuiUploader;
import ru.swayfarer.dropboxupdateuploader.helper.DropboxHelper;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.InjectionByCfg;
import ru.swayfarer.iocv3.annotation.LazyBean;
import ru.swayfarer.iocv3.annotation.OnApplicationStart;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.iocv3.bean.LateBean;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContent;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContentInfo;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;
import ru.swayfarer.swl3.updaterv3.refresher.UpdateRefresher;
import ru.swayfarer.swl3.updaterv3.scanner.UpdateScanContext;
import ru.swayfarer.swl3.updaterv3.scanner.UpdateScanner;

@LazyBean(false)
@Singleton
public class DropboxUpdaterService {
    
    @InjectionByCfg("ui.hide")
    public ObservableProperty<Boolean> hideUI;
    
    @InjectionByCfg("updater.localConentInfo")
    public UpdatableContentInfo localUpdatableContent;
    
    @InjectionByCfg("updater.rootDir")
    public FileSWL rootDir;
    
    @Injection
    public UpdateRefresher updateRefresher;
    
    @Injection
    public UpdateScanner updateScanner;
    
    @Injection
    public LateBean<GuiUploader> guiUploader;
    
    @Injection
    public DbxClientV2 dropboxClient;
    
    @Injection
    public DropboxHelper dropboxHelper;
    
    @Injection
    public SwconfIO swconfIO;

    @Injection
    public ExceptionsHandler exceptionsHandler;
    
    public ILogger logger = LogFactory.getLogger();
    
    @OnApplicationStart
    public void onApplicationStart(String... args)
    {
        var argsList = CollectionsSWL.list(args);
        
        var silent = false;
        
        silent = argsList.containsSome("-s", "--silent");
        
        if (argsList.containsSome("-r", "--remeber"))
        {
            hideUI.setValue(silent);
        }
        
        if (!silent || !hideUI.getValue())
        {
            logger.info("Initializing UI...");
            initUI();
        }
        
        var localContent = readLocalUpdatableContent();
        var remoteContent = readRemoteUpdatableContent(localContent);
        
        logger.info("Readed local content with '" + localContent.getFiles().size() + "' files");
        logger.info("Readed remote content with '" + remoteContent.getFiles().size() + "' files");
        
        var refreshEvent = RefreshEvent.builder()
                .modelContent(localContent)
                .targetContent(remoteContent)
                .build()
        ;
        
        try
        {
            logger.info("Waiting for update complete...");
            updateRefresher.refreshTargetAll(refreshEvent);
            logger.info("Update completed! Uploading new content info");
            
            uploadRemoteUpdatableContentInfo(remoteContent);
            logger.info("Completed! All done!");
        }
        catch (Throwable e)
        {
            logger.error(e, "Error while refreshing remote content");
        }
    }
    
    public void uploadRemoteUpdatableContentInfo(@NonNull UpdatableContent updatableContent)
    {
        var updatableContentPath = dropboxHelper.getUpdatableContentPath();
        
        try
        {
            var uploader = dropboxHelper.uploadFileWithOverwrite(updatableContentPath);
            var dos = DataOutStream.of(uploader.getOutputStream());
            
            swconfIO.serialize(
                    updatableContent, 
                    dos, 
                    updatableContentPath
            );
        }
        catch (Throwable e)
        {
            exceptionsHandler.handle(e, "Error while uploading new remote content info");
        }
    }
    
    public UpdatableContent readLocalUpdatableContent()
    {
        var updateScanContext = UpdateScanContext.builder()
                .rootDir(rootDir)
                .contentInfo(localUpdatableContent)
                .build()
        ;
        
        return updateScanner.scan(updateScanContext);
    }
    
    public UpdatableContent readRemoteUpdatableContent(@NonNull UpdatableContent localUpdatableContent)
    {
        var updatableContentPath = dropboxHelper.getUpdatableContentPath();
        
        if (StringUtils.isBlank(updatableContentPath))
        {
            throw new IllegalStateException("Remote content path was set to null or blank string! Check your updater.yml 'contentPath' property!");
        }
        
        UpdatableContent ret = null;
        
        try
        {
            logger.info("Downloading remote content state from", updatableContentPath);
            var is = dropboxClient.files().download(updatableContentPath).getInputStream();
            
            ret = swconfIO.deserialize(
                    UpdatableContent.class, 
                    DataInStream.of(is), 
                    updatableContentPath
            );
        }
        catch (Throwable e)
        {
            logger.error(e, "Remote content file was can't be readed. Using empty base updatable content!");
        }

        if (ret == null)
        {
            ret = new UpdatableContent(localUpdatableContent.getUpdatableContentInfo(), new ExtendedList<>());
        }
        else
        {
            localUpdatableContent.setUpdatableContentInfo(ret.getUpdatableContentInfo());
        }
        
        return ret;
    }
    
    public void initUI()
    {
        if (!hideUI.getValue())
        {
            var gui = getGuiUploader();
            
            if (gui == null)
            {
                throw new IllegalStateException("GUI for uploader was not created!");
            }
            
            gui.setVisible(true);
        }
        else
        {
            logger.info("UI hidden by config property 'ui.hide'. Change it if you needs gui for updloader!");
        }
    }
    
    public GuiUploader getGuiUploader()
    {
        return guiUploader.apply();
    }
}
