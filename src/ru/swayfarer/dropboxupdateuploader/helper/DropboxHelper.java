package ru.swayfarer.dropboxupdateuploader.helper;

import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.UploadUploader;
import com.dropbox.core.v2.files.WriteMode;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.InjectionByCfg;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;

@Singleton
public class DropboxHelper {
    
    @InjectionByCfg("updater.remoteContentPath")
    public String updatableContentPath;
    
    @InjectionByCfg("dropbox.path")
    public String dropboxPath;
    
    @Injection
    public DbxClientV2 dropboxClient;
    
    public String getDropboxPath(@NonNull String fileName)
    {
        if (!dropboxPath.endsWith("/"))
        {
            fileName = "/" + fileName;
        }
        
        var result = dropboxPath + fileName;
        
        // Правильно склеиваем слэши
        
        if (!result.startsWith("/"))
            result = "/" + result;
        
        if (result.endsWith("/"))
            result = result.substring(0, result.length() - 1);
        
        // Заменяем все точки на _. потому, что дропбокс не разрешит использовать имена, начинающиеся с точки
        if (result.contains("."))
            result = result.replace(".", "_.");
        
        return result;
    }
    
    public String getDropboxPath(@NonNull FileEntry fileEntry)
    {
        return getDropboxPath(fileEntry.getPath());
    }
    
    public String getUpdatableContentPath() 
    {
        return getDropboxPath(updatableContentPath);
    }
    
    @SneakyThrows
    public UploadUploader uploadFileWithOverwrite(@NonNull String path)
    {
        return dropboxClient.files()
                    .uploadBuilder(path)
                        .withMode(WriteMode.OVERWRITE)
                        .start()
        ;
    }
    
    public String getDirectFileLink(@NonNull String dropboxFileLink)
    {
        dropboxFileLink = dropboxFileLink
                .replace("dropbox.com", "dl.dropboxusercontent.com")
        ;
        
        if (dropboxFileLink.endsWith("?dl=0"))
            dropboxFileLink = StringUtils.subString(0, -5, dropboxFileLink);
        
        return dropboxFileLink;
    }
}
