package ru.swayfarer.dropboxupdateuploader;

import ru.swayfarer.dropboxupdateuploader.exception.InvalidConfigurationException;
import ru.swayfarer.iocv3.annotation.IocV3Starter;
import ru.swayfarer.iocv3.exception.BeanInitializationException;
import ru.swayfarer.iocv3.exception.NoSuchBeanFoundException;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;

@IocV3Starter
public class DropboxUpdateUploader {

    public static void main(String[] args)
    {
        var contextExceptionsHandler = ExceptionsHandler.getOrCreateContextHandler();
        
        ContextHelper.configureExceptionsHandler(contextExceptionsHandler);
        
        contextExceptionsHandler.configure()
            .rule()
                .type(
                        NoSuchBeanFoundException.class, 
                        BeanInitializationException.class,
                        InvalidConfigurationException.class
                )
                .thanSkip()
                .throwEx()
            .logAny()
        ;
        
        ContextHelper.startApp(args);
    }
}
