package ru.swayfarer.dropboxupdateuploader.settings;

import ru.swayfarer.iocv3.annotation.ConfigurationSwconf2;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;

@ConfigurationSwconf2(
        prefix = "dropbox",
        location = "f:dropboxUpdater/dropbox.yml",
        autoSaveDelay = 500
)
public class DropboxSettings extends Swconf2Config {
    public String token = "";
    public String path = "";
}
