package ru.swayfarer.dropboxupdateuploader.settings;

import ru.swayfarer.iocv3.annotation.ConfigurationSwconf2;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;

@ConfigurationSwconf2(
        prefix = "ui",
        location = "f:dropboxUpdater/ui.yml",
        autoSaveDelay = 500
)
public class UiSettings extends Swconf2Config {
    public ObservableProperty<Boolean> hide = Observables.createProperty(false);
}
