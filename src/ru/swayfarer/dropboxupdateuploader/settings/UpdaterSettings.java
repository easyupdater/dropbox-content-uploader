package ru.swayfarer.dropboxupdateuploader.settings;

import ru.swayfarer.iocv3.annotation.ConfigurationSwconf2;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.string.FilteringList;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContentInfo;

@ConfigurationSwconf2(
        prefix = "updater",
        location = "f:dropboxUpdater/updater.yml",
        autoSaveDelay = 500
)
public class UpdaterSettings extends Swconf2Config{
    
    public FileSWL rootDir = FileSWL.of("dropboxUpdater/source");
    public boolean exitOnFail = true;
    public String remoteContentPath = "updatableContent.yml";
    public int maxUploaderThreads = 4;
    
    public UpdatableContentInfo localConentInfo = UpdatableContentInfo.builder()
            .name("Local content")
            .hashingType("MD5")
            .filtering(new FilteringList())
            .build()
    ;
}
