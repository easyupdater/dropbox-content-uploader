package ru.swayfarer.dropboxupdateuploader.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;

@SuppressWarnings("serial")
@Getter @Setter @Accessors(chain = true)
public class DropboxUpdateException extends RuntimeException {

    public FileEntry fileEntry;
    
    public DropboxUpdateException()
    {
        super();
    }

    public DropboxUpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public DropboxUpdateException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DropboxUpdateException(String message)
    {
        super(message);
    }

    public DropboxUpdateException(Throwable cause)
    {
        super(cause);
    }
}
