package ru.swayfarer.dropboxupdateuploader.io;

import java.io.IOException;

import com.dropbox.core.RetryException;
import com.dropbox.core.v2.DbxClientV2;

import ru.swayfarer.dropboxupdateuploader.exception.DropboxUpdateException;
import ru.swayfarer.dropboxupdateuploader.helper.DropboxHelper;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.thread.ThreadsUtils;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.io.IFileEntryUploader;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;

@Singleton
public class ContentUploader implements IFileEntryUploader {
    
    public ILogger logger = LogFactory.getLogger();
    
    @Injection
    public DbxClientV2 dropboxClient;
    
    @Injection
    public DropboxHelper dropboxHelper;
    
    @Injection
    public ExceptionsHandler exceptionsHandler;
    
    @SuppressWarnings("deprecation")
    @Override
    public String upload(FileEntry fileEntry, IFunction0<DataInStream> streamSource, RefreshEvent refreshEvent) throws Throwable
    {
        for (;;)
        {
            try
            {
                if (!fileEntry.isDirectory())
                {
                    var entryPath = dropboxHelper.getDropboxPath(fileEntry);
                    var is = streamSource.apply();
                    
                    logger.info("Uploading entry path", entryPath);
                    
                    if (is == null)
                        throw new IOException("Can't open input stream for " + fileEntry.getPath());
                    
                    var uploader = dropboxHelper.uploadFileWithOverwrite(entryPath);
                    
                    uploader.uploadAndFinish(is);
                    
                    var sharedLink = dropboxClient.sharing().createSharedLink(entryPath);
                    
                    var sharedFileUrl = sharedLink.getUrl();
                    return "url:" + dropboxHelper.getDirectFileLink(sharedFileUrl);
                }
                
                break;
            }
            catch (RetryException retryException)
            {
                logger.info("Recieved retry exception for file '" + fileEntry.getPath() + "'. Waiting " + retryException.getBackoffMillis() + "' milisis...");
                ThreadsUtils.sleep(retryException.getBackoffMillis());
            }
            catch (Throwable e)
            {
                var dbxUpdateException = new DropboxUpdateException(e);
                dbxUpdateException.setFileEntry(fileEntry);
                exceptionsHandler.handle(dbxUpdateException, "Error while handling entry " + fileEntry.getPath());
                break;
            }
        }
        
        return null;
    }
}
