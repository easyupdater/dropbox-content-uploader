package ru.swayfarer.dropboxupdateuploader.io;

import com.dropbox.core.RetryException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.DeleteErrorException;

import lombok.NonNull;
import ru.swayfarer.dropboxupdateuploader.exception.DropboxUpdateException;
import ru.swayfarer.dropboxupdateuploader.helper.DropboxHelper;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.thread.ThreadsUtils;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.io.IFileEntryRemover;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;

@Singleton
public class ContentRemover implements IFileEntryRemover {

    public ILogger logger = LogFactory.getLogger();
    
    @Injection
    public DbxClientV2 dropboxClient;
    
    @Injection
    public DropboxHelper dropboxHelper;
    
    @Injection
    public ExceptionsHandler exceptionsHandler;
    
    @SuppressWarnings("deprecation")
    @Override
    public void remove(FileEntry fileEntry, @NonNull RefreshEvent refreshEvent) throws Throwable
    {
        var entryPath = dropboxHelper.getDropboxPath(fileEntry);
        
        Throwable lastRemoveException = null;
        
        try
        {
            logger.info("Removing '" + entryPath +  "' at remote");
            dropboxClient.files().delete(entryPath);
            lastRemoveException = null;
        }
        catch (RetryException retryException)
        {
            logger.info("Recieved retry exception for file '" + fileEntry.getPath() + "'. Waiting " + retryException.getBackoffMillis() + "' milisis...");
            ThreadsUtils.sleep(retryException.getBackoffMillis());
        }
        catch (Throwable e)
        {
            lastRemoveException = e;
        }
        
        if (lastRemoveException != null && !(lastRemoveException instanceof DeleteErrorException))
        {
            var dbxUpdateException = new DropboxUpdateException("for " + fileEntry.getPath(), lastRemoveException);
            dbxUpdateException.setFileEntry(fileEntry);
            throw dbxUpdateException;
        }
    }
}
