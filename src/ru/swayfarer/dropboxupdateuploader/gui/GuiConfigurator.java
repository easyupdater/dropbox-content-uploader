package ru.swayfarer.dropboxupdateuploader.gui;

import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JOptionPane;

import lombok.NonNull;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.InjectionByCfg;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.swing.utils.SwingUtils;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;

@Singleton
public class GuiConfigurator {

    @Injection
    public UpdaterContext updaterContext;
    
    @InjectionByCfg("updater.exitOnFail")
    public boolean exitOnClose;
    
    public GuiUploader configure(@NonNull GuiUploader guiUploader) 
    {
        subscribeToScan(guiUploader);
        subscribeToUpload(guiUploader);
        
        return guiUploader;
    }
    
    public void subscribeToUpload(@NonNull GuiUploader guiUploader)
    {
        var events = updaterContext.getEvents();
        var totalFilesToUpload = new AtomicInteger();
        
        events.eventContentStart.subscribe((updatableContent) -> {
            SwingUtils.exec(() -> {
                guiUploader.setHeaderText("Uploading content to dropbox...");
                guiUploader.setProgressValue(0);
                
                var fileToUpdateSize = (int) updatableContent.getFiles().exStream()
                        .not().filter(FileEntry::isDirectory)
                        .count()
                ;
                
                guiUploader.setMaxProgressValue(fileToUpdateSize);
                totalFilesToUpload.set(fileToUpdateSize);
            });
        });
        
        var totalFilesCount = new AtomicInteger(0);
        
        events.eventFileComplete.subscribe((e) -> {
            SwingUtils.exec(() -> {
                int value = totalFilesCount.getAndIncrement();
                var maxValue = totalFilesToUpload.get();
                
                guiUploader.setHeaderText("Uploading content to dropbox (" + value + "/" + maxValue + ")");
                guiUploader.setProgressValue(value);
                guiUploader.pbUploadProgress.repaint();
            });
        });
        
        events.eventFileFail.subscribe((fileEntryException) -> {
            SwingUtils.exec(() -> {
                var fileEntry = fileEntryException.getFileEntry();
                guiUploader.incrementProgressValue();
                
                JOptionPane.showMessageDialog(
                        guiUploader, 
                        "Error occured while updating file entry: \n" + fileEntry.getPath() + " \n" + fileEntryException.toString()
                );
                
                if (exitOnClose)
                {
                    System.exit(1);
                }
            });
        });
        
        events.eventContentEnd.subscribe((e) -> {
            SwingUtils.exec(() -> {
                guiUploader.setHeaderText("Completed!");
            });
        });
    }
    
    public void subscribeToScan(@NonNull GuiUploader guiUploader)
    {
        var scanEvents = updaterContext.getScanEvents();
        
        scanEvents.eventScanStart.subscribe((scanStartEvent) -> {
            SwingUtils.exec(() -> {
                var filesToScan = scanStartEvent.getFilesToScan();
                var rootDir = scanStartEvent.getScanContext().getRootDir();
                        
                guiUploader.setHeaderText("Scanning dir '" + rootDir.getName() + "' ...");
                guiUploader.setMaxProgressValue(filesToScan.size());
                guiUploader.setProgressValue(0);
            });
        });
        
        scanEvents.eventFileEntryScanned.subscribe((fileEntryScannedEvent) -> {
            SwingUtils.exec(() -> {
                guiUploader.incrementProgressValue();
            });
        });
    }
    
}
