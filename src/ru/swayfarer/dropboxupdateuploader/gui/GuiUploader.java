package ru.swayfarer.dropboxupdateuploader.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.OnInit;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.swing.box.JVBox;
import ru.swayfarer.swl3.swing.constants.ContentAlignment;
import ru.swayfarer.swl3.swing.utils.SwingUtils;

@Singleton
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("serial")
public class GuiUploader extends JFrame{
    
    @Injection
    public GuiConfigurator guiConfigurator;
    
    public JVBox boxContent = new JVBox();
    public JProgressBar pbUploadProgress = new JProgressBar();
    public JLabel lblHeader = new JLabel();
    
    public GuiUploader()
    {
        this.setSize(800, 100);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        add(boxContent, BorderLayout.CENTER);
        
        SwingUtils.alignment().text().set(lblHeader, ContentAlignment.HCENTER, ContentAlignment.VCENTER);
        SwingUtils.size().fix(lblHeader, -1, 50);
        SwingUtils.size().fix(pbUploadProgress, -1, 35);
        
        boxContent.addItem(lblHeader);
        boxContent.addItem(pbUploadProgress);
    }
    
    public GuiUploader setMaxProgressValue(int maxProgress) 
    {
        pbUploadProgress.setMaximum(maxProgress);
        return this;
    }
    
    public GuiUploader incrementProgressValue()
    {
        return setProgressValue(pbUploadProgress.getValue() + 1);
    }
    
    public GuiUploader setProgressValue(int progress)
    {
        pbUploadProgress.setValue(progress);
        return this;
    }
    
    public GuiUploader setHeaderText(@NonNull Object text) 
    {
        var str = text.toString();
        lblHeader.setText(str);
        
        return this;
    }
    
    @OnInit
    public void onBeanInit()
    {
        guiConfigurator.configure(this);
    }
    
    public static void main(String[] args)
    {
        var uploader = new GuiUploader();
        uploader.setVisible(true);
        uploader.setHeaderText("Hello!");
        
        uploader.setMaxProgressValue(100);
        uploader.setProgressValue(50);
    }
}
