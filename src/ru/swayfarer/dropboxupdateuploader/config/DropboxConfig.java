package ru.swayfarer.dropboxupdateuploader.config;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;

import lombok.SneakyThrows;
import ru.swayfarer.dropboxupdateuploader.exception.InvalidConfigurationException;
import ru.swayfarer.iocv3.annotation.ContextConfiguration;
import ru.swayfarer.iocv3.annotation.InjectionByCfg;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.exception.ExceptionsUtils;

@ContextConfiguration
public class DropboxConfig {
    
    @Singleton
    public DbxClientV2 dropboxClient(
            @InjectionByCfg("dropbox.token") String dropboxToken,
            @InjectionByCfg("dropbox.path") String dropboxPath
    )
    {
        ExceptionsUtils.IfEmpty(
                dropboxPath, 
                InvalidConfigurationException.class, 
                "Dropbox path was not set in configuration!"
        );
        
        ExceptionsUtils.IfEmpty(
                dropboxToken, 
                InvalidConfigurationException.class, 
                "Dropbox token was not set in configuration!"
        );
        
        DbxRequestConfig config = DbxRequestConfig.newBuilder(dropboxPath).build();
        DbxClientV2 client = new DbxClientV2(config, dropboxToken);
        return client;
    }
    
    @SneakyThrows
    @Singleton
    public FullAccount dropboxAccount(DbxClientV2 dropboxClient)
    {
        return dropboxClient.users().getCurrentAccount();
    }
}
