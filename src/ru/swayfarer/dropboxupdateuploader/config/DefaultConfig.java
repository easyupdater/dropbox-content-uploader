package ru.swayfarer.dropboxupdateuploader.config;

import ru.swayfarer.iocv3.annotation.ContextConfiguration;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.crypto.CryptoUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;

@ContextConfiguration
public class DefaultConfig {
    
    @Singleton
    public ExceptionsHandler exceptionsHandler()
    {
        return new ExceptionsHandler();
    }
    
    @Singleton
    public RLUtils rlUtils()
    {
        return RLUtils.defaultUtils();
    }
    
    @Singleton
    public CryptoUtils cryptoUtils()
    {
        return new CryptoUtils();
    }
    
    @Singleton
    public SwconfIO swconfIO()
    {
        return new SwconfIO();
    }
}
