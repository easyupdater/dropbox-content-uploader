package ru.swayfarer.dropboxupdateuploader.config;

import java.util.concurrent.Executor;

import ru.swayfarer.dropboxupdateuploader.io.ContentRemover;
import ru.swayfarer.dropboxupdateuploader.io.ContentUploader;
import ru.swayfarer.dropboxupdateuploader.settings.UpdaterSettings;
import ru.swayfarer.iocv3.annotation.ContextConfiguration;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.crypto.CryptoUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.thread.executor.queued.QueuedExecutor;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;
import ru.swayfarer.swl3.updaterv3.refresher.UpdateRefresher;
import ru.swayfarer.swl3.updaterv3.scanner.UpdateScanner;

@ContextConfiguration
public class UpdaterConfig {
    
    @Singleton
    public UpdaterContext updaterContext(
            RLUtils rlUtils, 
            CryptoUtils cryptoUtils,
            ExceptionsHandler exceptionsHandler,
            ContentUploader contentUploader,
            ContentRemover contentRemover
    )
    {
        return UpdaterContext.builder()
                .cryptoUtils(cryptoUtils)
                .rlUtils(rlUtils)
                .exceptionsHandler(exceptionsHandler)
                .fileEntryUploader(contentUploader)
                .fileEntryRemover(contentRemover)
                .build()
        ;
    }
    
    @Singleton
    public UpdateScanner updateScanner(UpdaterContext updaterContext)
    {
        return new UpdateScanner(updaterContext);
    }
    
    @Singleton
    public UpdateRefresher updateRefresher(UpdaterContext updaterContext, Executor updateRefresherExecutor)
    {
        var ret = new UpdateRefresher(updaterContext);
        ret.setActionExecutor((e) -> updateRefresherExecutor.execute(e.asJavaRunnable()));
        
        return ret;
    }
    
    @Singleton
    public Executor updateRefresherExecutor(UpdaterSettings updaterSettings)
    {
        var exec = new QueuedExecutor();
        exec.getSettings()
            .maxThreadsCount(updaterSettings.maxUploaderThreads)
        ;
        
        return exec;
    }
}
